import { assert } from 'chai';

import SpawnQueue from '../src/spawn-queue';

const TEST_SCRIPT = './test/sleep.sh';

console.log('starting tests at ', new Date());

describe('SpawnQueue', () => {
  describe('#addCommand', () => {
    it('should execute with 1 max', () => {
      const queue = new SpawnQueue({ max: 1 });
      queue.addCommand('sh', [TEST_SCRIPT]);
      assert(queue.busy.length === 1, 'command didn\'t get added to busy');
    });

    it('should not execute with 0 max', () => {
      const queue = new SpawnQueue({ max: 0 });
      queue.addCommand('sh', [TEST_SCRIPT]);
      assert(queue.queue.length === 1, 'command didn\'t get added to queue');
    });

    it('should not execute more than one with 1 max', () => {
      const queue = new SpawnQueue({ max: 1 });
      queue.addCommand('sh', [TEST_SCRIPT]);
      queue.addCommand('sh', [TEST_SCRIPT]);
      queue.addCommand('sh', [TEST_SCRIPT]);
      queue.addCommand('sh', [TEST_SCRIPT]);
      queue.addCommand('sh', [TEST_SCRIPT]);
      assert.isFalse(queue.busy.length < 1, 'command didn\'t get added to busy');
      assert.isFalse(queue.busy.length > 1, 'too many commands got added to busy');
      assert.isFalse(queue.queue.length < 4, 'not all remaining commands got added to queue');
      assert.isFalse(queue.queue.length > 4, 'too many commands got added to queue');
    });

    it('should not execute more than three with 3 max', () => {
      const queue = new SpawnQueue({ max: 3 });
      queue.addCommand('sh', [TEST_SCRIPT]);
      queue.addCommand('sh', [TEST_SCRIPT]);
      queue.addCommand('sh', [TEST_SCRIPT]);
      queue.addCommand('sh', [TEST_SCRIPT]);
      queue.addCommand('sh', [TEST_SCRIPT]);
      assert.isFalse(queue.busy.length < 3, 'command didn\'t get added to busy');
      assert.isFalse(queue.busy.length > 3, 'too many commands got added to busy');
      assert.isFalse(queue.queue.length < 2, 'not all remaining commands got added to queue');
      assert.isFalse(queue.queue.length > 2, 'too many commands got added to queue');
    });
  });
});
