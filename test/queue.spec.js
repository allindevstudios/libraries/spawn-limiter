import {
  assert,
} from 'chai';

import Queue, {
  makeTestPromise,
  makeTestPromiseArray,
} from '../src/queue';

describe('spawnLimiter', () => {
  describe('#makeTestPromise', () => {
    it('should return a promise', () => {
      const promise = makeTestPromise(200);
      assert(promise instanceof Promise, 'makePromise does not return a promise');
    });

    it('should resolve in the right time frame (~200ms)', () => {
      const start = new Date();
      return makeTestPromise(200).then(() => {
        const now = new Date();
        assert(now - start < 205, 'promise takes too long to resolve');
        assert(now - start > 195, 'promise takes too little time to resolve');
      });
    });

    it('should resolve in the right time frame (~400ms)', () => {
      const start = new Date();
      return makeTestPromise(400).then(() => {
        const now = new Date();
        assert(now - start < 405, 'promise takes too long to resolve');
        assert(now - start > 395, 'promise takes too little time to resolve');
      });
    });
  });

  describe('#makeTestPromiseArray', () => {
    it('should return a list of promises', () => {
      const promises = makeTestPromiseArray(5, 100, 120);
      promises.forEach((promise) => {
        assert(promise instanceof Promise, 'element is not a promise');
      });
    });

    it('the promises should resolve in time (~100-120 ms)', () => new Promise((resolve, reject) => {
      const start = new Date();
      const promises = makeTestPromiseArray(5, 100, 120);
      promises.forEach((promise) => {
        assert(promise instanceof Promise, 'element is not a promise');
        promise.then(() => {
          const now = new Date();
          assert(now - start < 125, 'promise takes too long to resolve');
          assert(now - start > 95, 'promise takes too little time to resolve');
        }).catch(err => reject(err));
      });
      Promise.all(promises).then(() => resolve());
    }));

    it('should handle 30 promises', () => new Promise((resolve, reject) => {
      const start = new Date();
      const promises = makeTestPromiseArray(30, 100, 120);
      promises.forEach((promise) => {
        assert(promise instanceof Promise, 'element is not a promise');
        promise.then(() => {
          const now = new Date();
          assert(now - start < 125, 'promise takes too long to resolve');
          assert(now - start > 95, 'promise takes too little time to resolve');
        }).catch(err => reject(err));
      });
      Promise.all(promises).then(() => resolve());
    }));
  });
});

describe('Queue', () => {
  describe('#addPromise', () => {
    it('should add a promise', () => {
      const promise = makeTestPromise(200);
      const queue = new Queue();
      queue.addPromise(promise);
      assert(queue.promises.length === 1, 'promise didn\'t get added to queue');
    });

    it('should add five promises', () => {
      const queue = new Queue();
      const amount = 5;

      for (let i = 0; i < amount; i += 1) {
        const promise = makeTestPromise(200);
        queue.addPromise(promise);
      }

      assert(queue.promises.length === amount, 'not all promises got added to queue');
    });

    it('should add twenty promises', () => {
      const queue = new Queue();
      const amount = 20;

      for (let i = 0; i < amount; i += 1) {
        const promise = makeTestPromise(200);
        queue.addPromise(promise);
      }

      assert(queue.promises.length === amount, 'not all promises got added to queue');
    });
  });
});