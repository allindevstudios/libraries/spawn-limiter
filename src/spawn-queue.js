import uuid4 from 'uuid/v4';
import { spawn } from 'child_process';

export default class SpawnQueue {
  constructor(props) {
    this.props = props;

    this.index = 0;


    this.max = props.max === undefined ? 1 : props.max;

    this.queue = [];
    this.busy = [];

    this.failed = [];
    this.completed = [];

    this.addCommand = this.addCommand.bind(this);
    this.executeCommand = this.executeCommand.bind(this);
    this.removeFromBusy = this.removeFromBusy.bind(this);
    this.update = this.update.bind(this);
  }

  setMax(newMax) {
    this.max = newMax;
  }

  getMax() {
    return this.max;
  }

  removeFromBusy(id) {
    this.busy = this.busy.filter(childProcess => childProcess.id !== id);
    this.update();
  }

  removeFromQueue(id) {
    this.queue = this.queue.filter(queuedProcess => queuedProcess.id !== id);
  }

  update() {
    if (this.busy.length < this.max) {
      if (this.queue.length > 0) {
        this.executeCommand(this.queue[0]);
        this.update();
      }
    }
  }

  executeCommand(queueCommand) {
    const childProcess = spawn(queueCommand.command, queueCommand.args, queueCommand.options);

    this.removeFromQueue(queueCommand.id);

    childProcess.on('close', (code) => {
      this.completed.push({
        ...queueCommand,
        code,
      });
      this.removeFromBusy(queueCommand.id);
    });

    childProcess.on('error', (err) => {
      this.failed.push({
        ...queueCommand,
        err,
      });
      this.removeFromBusy(queueCommand.id);
    });

    this.busy.push({
      ...queueCommand,
      process: childProcess,
    });
  }

  addCommand(command, args, options) {
    this.queue.push({
      command,
      args,
      options,
      id: uuid4(),
    });
    this.update();
  }
}

