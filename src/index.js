import SpawnQueue from './spawn-queue';

import Queue from './queue';

export default {
  SpawnQueue,
  Queue,
}