import _ from 'lodash';

export const makeTestPromise = (promiseTime) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, promiseTime);
  });
};

export const makeTestPromiseArray = (amount, minTime, maxTime) => {
  const range = _.range(amount);
  return range.map(() => {
    const duration = minTime + (Math.random() * (maxTime - minTime));
    return makeTestPromise(duration);
  });
};

export default class Queue {
  constructor(props) {
    this.props = props;

    this.index = 0;
    this.promises = [];

    this.increaseIndex = this.increaseIndex.bind(this);
  }

  increaseIndex() {
    this.index += 1;
  }

  addPromise(promise) {
    this.promises.push(promise);
    promise.then(() => this.increaseIndex());
  }
}
