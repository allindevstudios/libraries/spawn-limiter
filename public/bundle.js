'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var uuid4 = _interopDefault(require('uuid/v4'));
var child_process = require('child_process');
var _ = _interopDefault(require('lodash'));

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();







var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var SpawnQueue = function () {
  function SpawnQueue(props) {
    classCallCheck(this, SpawnQueue);

    this.props = props;

    this.index = 0;

    this.max = props.max === undefined ? 1 : props.max;

    this.queue = [];
    this.busy = [];

    this.failed = [];
    this.completed = [];

    this.addCommand = this.addCommand.bind(this);
    this.executeCommand = this.executeCommand.bind(this);
    this.removeFromBusy = this.removeFromBusy.bind(this);
    this.update = this.update.bind(this);
  }

  createClass(SpawnQueue, [{
    key: 'setMax',
    value: function setMax(newMax) {
      this.max = newMax;
    }
  }, {
    key: 'getMax',
    value: function getMax() {
      return this.max;
    }
  }, {
    key: 'removeFromBusy',
    value: function removeFromBusy(id) {
      this.busy = this.busy.filter(function (childProcess) {
        return childProcess.id !== id;
      });
      this.update();
    }
  }, {
    key: 'removeFromQueue',
    value: function removeFromQueue(id) {
      this.queue = this.queue.filter(function (queuedProcess) {
        return queuedProcess.id !== id;
      });
    }
  }, {
    key: 'update',
    value: function update() {
      if (this.busy.length < this.max) {
        if (this.queue.length > 0) {
          this.executeCommand(this.queue[0]);
          this.update();
        }
      }
    }
  }, {
    key: 'executeCommand',
    value: function executeCommand(queueCommand) {
      var _this = this;

      var childProcess = child_process.spawn(queueCommand.command, queueCommand.args, queueCommand.options);

      this.removeFromQueue(queueCommand.id);

      childProcess.on('close', function (code) {
        _this.completed.push(_extends({}, queueCommand, {
          code: code
        }));
        _this.removeFromBusy(queueCommand.id);
      });

      childProcess.on('error', function (err) {
        _this.failed.push(_extends({}, queueCommand, {
          err: err
        }));
        _this.removeFromBusy(queueCommand.id);
      });

      this.busy.push(_extends({}, queueCommand, {
        process: childProcess
      }));
    }
  }, {
    key: 'addCommand',
    value: function addCommand(command, args, options) {
      this.queue.push({
        command: command,
        args: args,
        options: options,
        id: uuid4()
      });
      this.update();
    }
  }]);
  return SpawnQueue;
}();

var Queue = function () {
  function Queue(props) {
    classCallCheck(this, Queue);

    this.props = props;

    this.index = 0;
    this.promises = [];

    this.increaseIndex = this.increaseIndex.bind(this);
  }

  createClass(Queue, [{
    key: 'increaseIndex',
    value: function increaseIndex() {
      this.index += 1;
    }
  }, {
    key: 'addPromise',
    value: function addPromise(promise) {
      var _this = this;

      this.promises.push(promise);
      promise.then(function () {
        return _this.increaseIndex();
      });
    }
  }]);
  return Queue;
}();

var index = {
  SpawnQueue: SpawnQueue,
  Queue: Queue
};

module.exports = index;
