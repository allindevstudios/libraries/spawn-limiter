import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';

export default {
  moduleName: 'file-helpers',
  entry: 'src/index.js',
  dest: 'public/bundle.js',
  format: 'cjs', // immediately-invoked function expression — suitable for <script> tags
  plugins: [
    resolve({
    // pass custom options to the resolve plugin
      customResolveOptions: {
        moduleDirectory: 'node_modules',
      },
    }),
    babel({
      exclude: 'node_modules/**',
    }),
  ],
  external: ['child_process', 'uuid/v4', 'lodash'],
};
